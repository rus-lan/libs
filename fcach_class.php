<?php
class Cach{

    protected static $_instance;
    private $active = true;
    private $dir = '';
    private $format = '.cash';

	//старт файла
	private function __construct(){}
	//запрещаем клонирование объекта модификатором private
	private function __clone() {}
	//запрещаем клонирование объекта модификатором private
	private function __wakeup() {}

	public static function getInstance( string $host = '127.0.0.1', int $port = 11211, string $dir ): Cach {
		//инициалезируем обьект
		if (self::$_instance === null) self::$_instance = new self;
        self::$_instance->dir = $dir;
        self::$_instance->active = (bool) (!file_exists($dir))? (
            mkdir($dir)? chmod($dir, 0777) : false
        ) : true;
		//возвращаем подключение
		return self::$_instance;
	}

    //запись значение
    private static function add(string $key, $obj): bool{
        $location = self::$_instance->dir;
        $key = explode('.', $key);
        for ($i=0; $i < ( count($key) - 1); $i++ ){
            $location .= $key[$i];
            if(!file_exists($location)) {
                mkdir($location);
                chmod($location, 0777);
            }
            $location .= '/';
        }
        $location .= $key[count($key) - 1].self::$_instance->format;
        return file_put_contents($location, serialize($obj))? chmod($location, 0777) : false;
    }

    //устанавливает значение
    public static function set(string $key, int $time, callable $callable){
        return (self::$_instance->active AND $time) ? (
            ( ! $res = self::get($key, $time) ) ? (
                ( $res = $callable() AND self::add( $key, $res ) ) ?
                    $res : false
            ): $res
        ) : $callable();
    }

    //извлекает значение по ключу
    public static function get(string $key, int $time = 60*60 ){
        $location = self::$_instance->dir.str_replace('.', '/', $key).self::$_instance->format;
        return (self::$_instance->active AND file_exists($location) AND
            $time AND ( $time > ( microtime(true) - filemtime($location) ) ) ) ?
            unserialize ( file_get_contents( $location ) ) : false;
    }

    //заменяет кэш
    public static function replace(string $key, int $time, callable $callable){
        self::del( $key );
        return (self::$_instance->active AND $time) ? (
        ( $res = $callable() AND self::add( $key, $res ) ) ?
            $res : false
        ) : $callable();
    }

    private static function removeDir( string $dir ): bool {
        if($objs = glob($dir."/*"))
            foreach($objs as $obj)
                is_dir($obj)? self::removeDir($obj) : unlink($obj);
        rmdir($dir);
        return true;
    }

    //удаляет значение
    public static function del(string $key): bool {
        $location = self::$_instance->dir.str_replace('.', '/', $key);
        return (self::$_instance->active)? (
            is_file($location.self::$_instance->format)? unlink($location.self::$_instance->format) : (
                ( substr( $location, -1 ) === '*' )? self::removeDir( substr($location, 0, -2) ) : self::removeDir($location)
            )
        ) : false;
    }

    //удаляет все закэшированные данные
    public static function flush(): bool{
        return (self::$_instance->active)? (
            self::removeDir( substr(self::$_instance->dir, 0, -1) )
        ) : false;
    }

    //запрос с возвратом ответа
    public static function has(): bool {
        return self::$_instance->active;
    }

	//Закрытие соединения
	public function __destruct(){}
}