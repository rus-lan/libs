<?php
class Ajax{
	//обьект
    protected static $_instance; 
	//старт файла
	public function __construct(){}
	//запрещаем клонирование объекта модификатором private
	private function __clone() {}
	//запрещаем клонирование объекта модификатором private
	private function __wakeup() {}
	//обьявляем
	public static function getInstance() {
		//инициалезируем обьект
		if (self::$_instance === null) self::$_instance = new self; 
		//параметры
		self::$_instance->ajax = false;
		//возвращаем подключение
		return self::$_instance;
	}
	
	//поиск и запуск распознания подгружаемых блоков
	public static function get( $pars = '' ){
		echo (is_array($pars) or is_object($pars))?json_encode($pars):$pars;
		self::$_instance->ajax = true;
		return true;
	}
	//поиск и запуск распознания подгружаемых блоков
	public static function is_get(){ return self::$_instance->ajax; }
}
