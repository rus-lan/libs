<?php
class URL{
	//обьект
    protected static $_instance; 
	//старт файла
	private function __construct(){
		$this->url 		= parse_url(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'/');
		$this->port 	= ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']!='off') || 443==$_SERVER['SERVER_PORT'])?'https':'http';
		$this->host 	= isset($_SERVER['SERVER_NAME'])?$_SERVER['SERVER_NAME']:$_SERVER['HTTP_HOST'];
	}
	//запрещаем клонирование объекта модификатором private
	private function __clone() {}
	//запрещаем клонирование объекта модификатором private
	private function __wakeup() {}
	//обьявляем
	public static function getInstance( $langin=array() ) {
		//инициалезируем обьект
		if (self::$_instance === null) self::$_instance = new self;  
		self::$_instance->post 	= self::getPathArray(self::$_instance->url, $langin);
        self::$_instance->post  = self::$_instance->post?self::$_instance->post:[];
		self::$_instance->get 	= self::getQueryArray(self::$_instance->url);
        self::$_instance->get   = self::$_instance->get?self::$_instance->get:[];
		//возвращаем подключение
		return self::$_instance;
	}
	//запрос с возвратом ответа
	private static function getPathArray($url=array(), $langin=array()){
		//обрабатываем полученные данные url
		$var = explode('/', isset($url['path'])?$url['path']:'');
		$varAr = array_values(array_filter( $var ));

		//определение языка интерфейса
		if(isset($_COOKIE['lang']) and in_array($_COOKIE['lang'], $langin)) 																	//язык в куках
			self::$_instance->lang = $_COOKIE['lang'];
		elseif(isset($varAr[0]) and in_array($varAr[0], $langin)){ 																				//язык в url
			self::$_instance->lang = $varAr[0];
			$varAr[0] = '';
			$varAr = array_values(array_filter( $varAr ));
			URL::url( array( 'path' => '/'.implode('/', $varAr) ) );
		}elseif(isset($_SERVER["HTTP_HOST"]) and $host = explode('.', $_SERVER["HTTP_HOST"]) and 
				( isset($host[0]) and $host[0]!='www' and in_array($host[0], $langin) or isset($host[1]) and in_array($host[1], $langin) ) ) 	//язык в поддомене
			self::$_instance->lang = ($host[0]=='www')?$host[1]:$host[0];
		elseif(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) and in_array(mb_substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2), $langin)) 				//язык в параметрах сервера
			self::$_instance->lang = mb_substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
		else // применяем стандарт
			self::$_instance->lang = (current($langin))?current($langin):'en';

		//если не существует
		$zapret = array(":",";","^",'"',"'","<",">","|","/","\\","*","--","$","[","]","{","}");
		for ($i=0; isset($varAr[$i]); $i++) $varAr[$i] = str_replace($zapret, "",$varAr[$i]);
		
		//вывод результата
		return array_values(array_filter($varAr));
	}
	//запрос с возвратом ответа
	private static function getQueryArray($url=array()){
		$var = isset($url['query'])?$url['query']:false;
		if(!$var) return false; else $url = array();
		$varArAb = array_values(array_filter(explode('&', $var)));
		$zapret = array(":",";","^",'"',"'","<",">","|","/","\\","*","--","$","[","]","{","}");
		for ($i=0; isset($varArAb[$i]); $i++) { 
			list($key, $val) = explode('=', str_replace($zapret, "",$varArAb[$i]));
			if($key) $url[$key] = $val;
		}
		return array_filter($url);
	}

	//returns
	public static function url(array  $arr = []): array{
		if( isset($arr[0]) ) foreach ($arr as $key => $value) self::$_instance->url[$key] = $value;
		return self::$_instance->url;
	}
	public static function post(array  $arr = []): array{
		if( isset($arr[0]) ) foreach ($arr as $key => $value) self::$_instance->post[$key] = $value;
		return self::$_instance->post;
	}
	public static function get(array  $arr = []): array {
		if( isset($arr[0]) ) foreach ($arr as $key => $value) self::$_instance->get[$key] = $value;
		return self::$_instance->get;
	}
	public static function host(): string { return self::$_instance->host; }
	public static function port(): string { return self::$_instance->port; }
	public static function lang(): string { return self::$_instance->lang; }
	//Закрытие соединения
	public function __destruct(){ self::$_instance = null; }
}
