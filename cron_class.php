<?php
class CRON{
	//обьект
    protected static $_instance; 
	//старт файла
	public function __construct(){}
	//запрещаем клонирование объекта модификатором private
	private function __clone() {}
	//запрещаем клонирование объекта модификатором private
	private function __wakeup() {}
	//обьявляем
	public static function getInstance( array $parent = [] ): CRON {
		//инициалезируем обьект
		if (self::$_instance === null) self::$_instance = new self;  
		self::$_instance->parent 		= [];
		self::$_instance->controller 	= '';
		self::$_instance->function 		= '';
		for ($i=0; isset($parent[$i]); $i++) { 
			if($i==1){
                $var = explode(':', $parent[$i]);
                if(!isset($var[2])){
                    self::$_instance->controller = isset($var[0])?$var[0]:'help';
                    self::$_instance->function = isset($var[1])?$var[1]:'help';
                }else{
                    self::$_instance->controller = 'help';
                    self::$_instance->function = 'help';
                }
			}elseif($i)
				self::$_instance->parent[] = $parent[$i];
		}
		//возвращаем подключение
		return self::$_instance;
	}
	//вывод переменных
	public static function is_controller(): string{ return self::$_instance->controller; }
	public static function is_parent(): array{ return self::$_instance->parent; }
	public static function is_function(): string{ return self::$_instance->function; }
}
