<?php
class Router{
	//обьект
    protected static $_instance; 
	//старт файла
	public function __construct(){}
	//запрещаем клонирование объекта модификатором private
	private function __clone() {}
	//запрещаем клонирование объекта модификатором private
	private function __wakeup() {}
	//обьявляем
	public static function getInstance( ) {
		//инициалезируем обьект
		if (self::$_instance === null) self::$_instance = new self;
		self::$_instance->query 		= (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest')?'ajax':'html';
		self::$_instance->path 			= URL::url()['path'];
		self::$_instance->function 		= false;
		self::$_instance->cache 		= false;
		self::$_instance->return 		= false;
		self::$_instance->controller 	= false;
		//возвращаем подключение
		return self::$_instance;
	}

	private static function getParent($matches){
		self::$_instance->parent = [];
		for ($i=0; isset($matches[$i]); $i++)
			if(substr($matches[$i],0,1)!='/' and substr($matches[$i],-1)!='/' and strlen($matches[$i])>0)
				self::$_instance->parent[] = $matches[$i];
		return true;
	}

	public static function get($path = '/', $controller = false, $where = array(), $cache = 0){
		$query = 'html';
		if(self::$_instance->query == $query AND (!self::$_instance->return OR $path===self::$_instance->path) ){
			foreach ($where as $key => $value)
				$path 	= str_replace("{$key}", "($value)", $path);
			$path 		= "|^".str_replace(['/','{','}'], ['\/','',''], $path)."$|";
			if( preg_match( $path, self::$_instance->path ) ){
				$controller 		= explode('@', $controller);
				self::$_instance->controller 	= isset($controller[0])?$controller[0]:'startController';
				self::$_instance->function 		= isset($controller[1])?$controller[1]:'index';
				self::$_instance->cache 		= ($cache>0)?($cache+0):0;
				preg_replace_callback( $path, 'Router::getParent', self::$_instance->path );
				//именновайный массив с параметрами
				if(count($where) == count(self::$_instance->parent)){
					$i=0;
					foreach ($where as $key => $value) {
						$where[$key] = self::$_instance->parent[$i];
						$i++;
					}
					self::$_instance->parent 	= $where;
				}
				//то что результат подобран
				self::$_instance->return 		= true;
				return self::$_instance->return;
			}
		}
		return false;
	}
	public static function post($path = '/', $controller = false, $where = array(), $cache = 0){
		$query = 'ajax';
		if(self::$_instance->query == $query AND (!self::$_instance->return OR $path===self::$_instance->path) ){
			foreach ($where as $key => $value)
				$path 	= str_replace("{$key}", "($value)", $path);
			$path 		= "|^".str_replace(['/','{','}'], ['\/','',''], $path)."$|";
			if( preg_match( $path, self::$_instance->path ) ){
				$controller 		= explode('@', $controller);
				self::$_instance->controller 	= isset($controller[0])?$controller[0]:'startController';
				self::$_instance->function 		= isset($controller[1])?$controller[1]:'index';
				self::$_instance->cache 		= ($cache>0)?($cache+0):0;
				preg_replace_callback( $path, 'Router::getParent', self::$_instance->path );
				//именновайный массив с параметрами
				if(count($where) == count(self::$_instance->parent)){
					$i=0;
					foreach ($where as $key => $value) {
						$where[$key] = self::$_instance->parent[$i];
						$i++;
					}
					self::$_instance->parent 	= $where;
				}
				//то что результат подобран
				self::$_instance->return 		= true;
				return self::$_instance->return;
			}
		}
		return false;
	}
	public static function is_controller(): string { return self::$_instance->controller; }
	public static function is_function(): string { return self::$_instance->function; }
	public static function is_cache(): int { return self::$_instance->cache; }
	public static function is_parent(){ return (object) self::$_instance->parent; }
	//Закрытие соединения
	public function __destruct(){ self::$_instance = null; }
}
