<?php
class Cach{

    protected static $_instance;
    private $active = true;

	//старт файла
	private function __construct(){}
	//запрещаем клонирование объекта модификатором private
	private function __clone() {}
	//запрещаем клонирование объекта модификатором private
	private function __wakeup() {}

	public static function getInstance( string $host = '127.0.0.1', int $port = 11211, string $dir = '' ): Cach {
		//инициалезируем обьект
		if (self::$_instance === null) self::$_instance = new self;
		//переводим кодировку
		if ( class_exists('Memcache') ) {
			self::$_instance->memcache = new Memcache;
			self::$_instance->memcache->connect($host, $port)
                or self::$_instance->active = false;
		}else self::$_instance->active = false;
		//возвращаем подключение
		return self::$_instance;
	}

    //устанавливает значение
    public static function set(string $key, int $time, callable $callable){
        return (self::$_instance->active AND $time) ?
            ( ! $res = self::$_instance->memcache->get($key) ) ?
                ( $res = $callable() AND self::$_instance->memcache->set( $key, $res, MEMCACHE_COMPRESSED, $time ) ) ?
                    $res : false
                : $res
            : $callable();
    }

    //извлекает значение по ключу
    public static function get(string $key){
        return (self::$_instance->active) ?
            self::$_instance->memcache->get($key) : false;
    }

    //заменяет кэш
    public static function replace(string $key, int $time, callable $callable){
        return (self::$_instance->active AND $time) ?
            ( $res = $callable() AND self::$_instance->memcache->replace( $key, $res, MEMCACHE_COMPRESSED, $time ) ) ?
                $res : false
            : $callable();
    }

    //удаляет значение
    public static function del(string $key): bool {
        return (self::$_instance->active)?
            self::$_instance->memcache->delete($key) : false;
    }

    //удаляет все закэшированные данные
    public static function flush(): bool{
        return (self::$_instance->active)?
            self::$_instance->memcache->flush() : false;
    }

    //запрос с возвратом ответа
    public static function has(): bool {
        return self::$_instance->active;
    }

	//Закрытие соединения
	public function __destruct(){
		if(self::$_instance->active AND self::$_instance->memcache)
		    self::$_instance->memcache->close();
	}
}