<?php
class Template{
	//обьект
    protected static $_instance; 
	//старт файла
	public function __construct(){}
	//запрещаем клонирование объекта модификатором private
	private function __clone() {}
	//запрещаем клонирование объекта модификатором private
	private function __wakeup() {}
	//обьявляем
	public static function getInstance( $config=array() ) {
		//инициалезируем обьект
		if (self::$_instance === null) self::$_instance = new self;  
		self::$_instance->config 		= $config;
		self::$_instance->parent 		= array();
		//возвращаем подключение
		return self::$_instance;
	}

	private static function openFiles($file = false){
		return ($file)?((file_exists($file))? fread(fopen($file,'rb'),filesize($file)) : '') : '';
	}
	
	//поиск и запуск распознания подгружаемых блоков
	public static function get( $template=false, $parent = array(), $function = false ){
		if(!$template) return '';

		//подключаем файл с функциями различных CMS
		self::$_instance->base_dir 		= BASE_DIR.self::$_instance->config['app']['start'].'/'.BASE_PROJECT.'/';
        if($function and $function=='adm_cms'){
            self::$_instance->adm 			= self::$_instance->config['app']['adm']['start'].'/';
            self::$_instance->base_tpl 		= self::$_instance->base_dir.self::$_instance->adm.self::$_instance->config['app']['adm']['template'].'/';
            self::$_instance->base_func 	= self::$_instance->base_dir.self::$_instance->adm.self::$_instance->config['app']['adm']['func'].'/';
        }else{
            self::$_instance->base_tpl 		= self::$_instance->base_dir.self::$_instance->config['app']['template'].'/';
            self::$_instance->base_func 	= self::$_instance->base_dir.self::$_instance->config['app']['func'].'/';
        }

		if($function and file_exists(self::$_instance->base_func."$function.php")) 
			include_once self::$_instance->base_func."$function.php";
		//записываем параметры
		self::$_instance->parent = $parent;
		//проверка шаблона
		$template = self::$_instance->base_tpl."$template.php";
		//получаем шаблон и выводим содержимое
		return (file_exists($template))? include_once $template: false;
	}
	//вывод переменных
	public static function dir($tpl=false){ return ($tpl)?self::$_instance->base_tpl:self::$_instance->base_dir; }
	public static function config(){ return self::$_instance->config; }
	public static function parent(){ return self::$_instance->parent; }
	public static function prefix(){ return self::$_instance->adm; }
}
